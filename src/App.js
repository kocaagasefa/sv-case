import {
  Switch,
  BrowserRouter as Router,
  Route,
  Redirect,
} from "react-router-dom";
import configureStore from "./store";
import Layout from "./components/Layout";
import PostList from "./components/PostList";
import PostDetail from "./components/PostDetail";
import { Provider } from "react-redux";
import ProfileDetail from "./components/ProfileDetail";
const store = configureStore();
function App() {
  return (
    <Provider store={store}>
      <Router>
        <Layout>
          <Switch>
            <Route path="/post/:id">
              <PostDetail />
            </Route>
            <Route path="/profile/:id">
              <ProfileDetail />
            </Route>
            <Route path="/">
              <PostList />
            </Route>
            <Redirect to="/" />
          </Switch>
        </Layout>
      </Router>
    </Provider>
  );
}

export default App;
