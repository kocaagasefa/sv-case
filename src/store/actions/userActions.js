import { GET_USER } from "../../constants/actionTypes";

export function getUser(id) {
  return {
    type: GET_USER,
    payload: {
      request: {
        url: "/users/" + id,
      },
    },
  };
}
