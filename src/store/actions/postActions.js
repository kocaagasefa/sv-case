import {
  DELETE_POST,
  GET_POSTS,
  GET_POST,
  UPDATE_POST,
} from "../../constants/actionTypes";

export function getPosts() {
  return {
    type: GET_POSTS,
    payload: {
      request: {
        url: "/posts?_limit=6",
      },
    },
  };
}

export function getPost(id) {
  return {
    type: GET_POST,
    payload: {
      request: {
        url: `/posts/${id}`,
      },
    },
  };
}

export function updatePost({ id, ...post }) {
  return {
    type: UPDATE_POST,
    payload: {
      request: {
        url: `/posts/${id}`,
        method: "PUT",
        data: post,
      },
    },
  };
}

export function deletePost(id) {
  return {
    type: DELETE_POST,
    payload: {
      request: {
        url: `/posts/${id}`,
        method: "DELETE",
      },
      id,
    },
  };
}
