export const postLoadingSelector = (state) => state.posts.loading;
export const postListSelector = (state) => state.posts.list;
export const postsErrorSelector = (state) => state.posts.error;
