import {
  DELETE_POST,
  GET_POSTS,
  UPDATE_POST,
} from "../../constants/actionTypes";
import ActionCreator from "../../helpers/actionTypeCreate";

const initialState = {
  loading: false,
  error: null,
  list: [],
};

const postsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_POSTS:
      return {
        ...state,
        loading: true,
      };
    case ActionCreator.success(GET_POSTS):
      return {
        loading: false,
        list: action.payload.data,
      };
    case ActionCreator.fail(GET_POSTS):
      return {
        ...state,
        loading: false,
        error: action.error.message,
      };
    case ActionCreator.success(DELETE_POST):
      return {
        ...state,
        list: state.list.filter(
          (post) => post.id !== action.meta.previousAction.payload.id
        ),
      };
    case ActionCreator.success(UPDATE_POST):
      return {
        ...state,
        list: state.list.map((post) =>
          post.id !== action.payload.data.id ? post : action.payload.data
        ),
      };
    default:
      return state;
  }
};

export default postsReducer;
