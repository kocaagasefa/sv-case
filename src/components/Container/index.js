import styles from "./style.module.css";
import classNames from "classnames";
const Container = ({ children, className, ...props }) => {
  return (
    <div className={classNames(styles.container, className)} {...props}>
      {children}
    </div>
  );
};

export default Container;
