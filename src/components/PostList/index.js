import { List, Modal, Input, Form, Button } from "antd";
import { useForm } from "antd/lib/form/Form";
import { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  deletePost,
  getPosts,
  updatePost,
} from "../../store/actions/postActions";
import {
  postListSelector,
  postLoadingSelector,
  postsErrorSelector,
} from "../../store/selectors/postsSelector";
import PostListItem from "../PostListItem";
import styles from "./style.module.css";

const PostList = () => {
  const dispatch = useDispatch();
  const loading = useSelector(postLoadingSelector);
  const postList = useSelector(postListSelector);
  const error = useSelector(postsErrorSelector);
  const [updateForm] = useForm();
  const [showUpdateModalId, setShowUpdateModalId] = useState(null);
  const [confirmLoading, setConfirmLoading] = useState(false);

  useEffect(() => {
    dispatch(getPosts());
  }, [dispatch]);

  const onPostDelete = (postId) => {
    Modal.confirm({
      title: "Silmek istediğinizden emin misiniz?",
      okText: "Evet",
      cancelText: "İptal",
      onOk: () => {
        return dispatch(deletePost(postId));
      },
    });
  };

  const onSelectToUpdate = (post) => {
    updateForm.setFieldsValue({
      title: post.title,
      body: post.body,
    });
    setShowUpdateModalId(post.id);
  };

  const onPostUpdate = () => {
    setConfirmLoading(true);
    const newPost = {
      ...updateForm.getFieldsValue(),
      id: showUpdateModalId,
    };
    return dispatch(updatePost(newPost)).then((res) => {
      setShowUpdateModalId(null);
      setConfirmLoading(false);
    });
  };
  return (
    <>
      <List
        locale={{
          emptyText: error,
        }}
        className={styles["post-list"]}
        dataSource={postList}
        loading={loading}
        itemLayout="horizontal"
        renderItem={(item) => (
          <PostListItem
            data={item}
            key={item.id}
            onDelete={() => onPostDelete(item.id)}
            onUpdate={() => onSelectToUpdate(item)}
          />
        )}
      />
      <Modal
        title="Düzenle"
        onOk={onPostUpdate}
        visible={Boolean(showUpdateModalId)}
        onCancel={() => setShowUpdateModalId(null)}
        confirmLoading={confirmLoading}
        footer={[
          <div style={{ width: "100%", textAlign: "left" }}>
            <Button
              key="submit"
              className={styles.update}
              loading={confirmLoading}
              onClick={onPostUpdate}
            >
              GÜNCELLE
            </Button>
          </div>,
        ]}
      >
        <Form form={updateForm} layout="vertical">
          <Form.Item label="Title" name="title">
            <Input />
          </Form.Item>
          <Form.Item label="Body" name="body">
            <Input.TextArea autoSize={{ minRows: 3, maxRows: 5 }} />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default PostList;
