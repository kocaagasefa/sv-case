import { Row, Col, Button } from "antd";
import { Link } from "react-router-dom";
import Logo from "../../assets/images/logo.png";
import Container from "../Container";
import styles from "./style.module.css";

const Topbar = () => {
  return (
    <header className={styles.header}>
      <Container>
        <Row align="middle">
          <Col>
            <div className={styles.logo}>
              <img alt="sv case" src={Logo} />
            </div>
          </Col>
          <Col flex="auto">
            <Link to="/" className={styles["nav-link"]}>
              Posts
            </Link>
          </Col>
          <Col>
            <Button className={styles.login}>Login</Button>
          </Col>
        </Row>
      </Container>
    </header>
  );
};

export default Topbar;
