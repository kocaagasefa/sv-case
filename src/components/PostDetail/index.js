import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Alert, Descriptions, Skeleton } from "antd";
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";
import { getPost } from "../../store/actions/postActions";

const PostDetail = () => {
  const [post, setPost] = useState(null);
  const [loading, setLoading] = useState(false);
  const { id } = useParams();
  const dispatch = useDispatch();
  useEffect(() => {
    setLoading(true);
    dispatch(getPost(id)).then((res) => {
      setLoading(false);
      if (!res.error) {
        const newPost = res.payload.data;
        setPost(newPost);
      }
    });
  }, [dispatch, id]);
  if (loading) return <Skeleton />;
  if (!post) return <Alert message="Post alınamadı" type="error" />;

  return (
    <div>
      <Descriptions bordered column={1}>
        <Descriptions.Item label="Title">{post.title}</Descriptions.Item>
        <Descriptions.Item label="Body">{post.body}</Descriptions.Item>
        <Descriptions.Item label="User">
          <Link to={`/profile/${post.userId}`}>{post.userId}</Link>
        </Descriptions.Item>
      </Descriptions>
    </div>
  );
};

export default PostDetail;
