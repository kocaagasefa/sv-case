import { List, Row, Col, Space, Button, Typography } from "antd";
import { useHistory } from "react-router-dom";
import classNames from "classnames";
import styles from "./style.module.css";
const PostListItem = ({ data, onDelete, onUpdate }) => {
  const { push } = useHistory();
  const goToDetail = () => push(`/post/${data.id}`);

  return (
    <>
      <List.Item className={styles["post-list-item"]}>
        <Row align="middle" style={{ width: "100%" }}>
          <Col flex="auto">
            <strong className="mr3">{data.id}</strong>{" "}
            <span className={styles.title}>{data.title}</span>
          </Col>
          <Col>
            <Space size={20}>
              <Button
                onClick={goToDetail}
                className={classNames(styles.button, styles.detail)}
              >
                {" "}
                DETAY
              </Button>
              <Button
                onClick={onUpdate}
                className={classNames(styles.button, styles.update)}
              >
                DÜZENLE
              </Button>
              <Button
                onClick={onDelete}
                className={classNames(styles.button, styles.delete)}
              >
                SİL
              </Button>
            </Space>
          </Col>
        </Row>
      </List.Item>
    </>
  );
};

export default PostListItem;
