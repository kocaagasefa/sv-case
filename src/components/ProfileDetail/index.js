import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Alert, Typography, Skeleton, Row, Col, Descriptions } from "antd";
import { useParams } from "react-router-dom";
import Map from "../Map";
import { getUser } from "../../store/actions/userActions";
import styles from "./style.module.css";

const ProfileDetail = () => {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(false);
  const { id } = useParams();
  const dispatch = useDispatch();
  useEffect(() => {
    setLoading(true);
    dispatch(getUser(id)).then((res) => {
      setLoading(false);
      if (!res.error) {
        const newPost = res.payload.data;
        setUser(newPost);
      }
    });
  }, [dispatch, id]);
  if (loading) return <Skeleton />;
  if (!user) return <Alert message="Kullanıcı alınamadı" type="error" />;

  return (
    <div className={styles.profile}>
      <Row>
        <Col sm={12}>
          <Typography.Title level={2} className={styles.name}>
            {user.name}
          </Typography.Title>
          <Typography.Title level={5} className={styles.city}>
            {user.address?.city}
          </Typography.Title>
          <Descriptions column={1} className={styles["user-data"]}>
            <Descriptions.Item label="Username">
              {user.username}
            </Descriptions.Item>
            <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
            <Descriptions.Item label="Phone">{user.phone}</Descriptions.Item>
            <Descriptions.Item label="Website">
              <Typography.Link>{user.website}</Typography.Link>
            </Descriptions.Item>
            <Descriptions.Item label="Company">
              {user.company?.name}
            </Descriptions.Item>
          </Descriptions>
        </Col>
        <Col sm={12}>
          <Map
            position={{
              lat: +user.address.geo.lat,
              lng: +user.address.geo.lng,
            }}
          />
        </Col>
      </Row>
    </div>
  );
};

export default ProfileDetail;
