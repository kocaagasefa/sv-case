import Footer from "../Footer";
import Container from "../Container";
import Topbar from "../Topbar";
import styles from "./style.module.css";
const Layout = ({ children }) => {
  return (
    <div className={styles.layout}>
      <Topbar />
      <Container className={styles["content-container"]}>
        <div className={styles.content}>{children}</div>
      </Container>
      <Footer />
    </div>
  );
};

export default Layout;
