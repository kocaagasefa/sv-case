import { Typography, Row, Col, Divider, Space, Tooltip } from "antd";
import { Link } from "react-router-dom";
import Container from "../Container";
import {
  TwitterSquareFilled,
  FacebookFilled,
  DribbbleSquareFilled,
  GithubFilled,
} from "@ant-design/icons";
import styles from "./style.module.css";
import classNames from "classnames";
const Footer = () => {
  return (
    <footer>
      <Container>
        <Row align="middle" justify="space-between">
          <Col>
            <div>
              <Typography.Text
                className={classNames(styles.text, styles.important)}
              >
                Thank you for supporting us!
              </Typography.Text>
            </div>
            <div>
              <Typography.Text className={styles.text}>
                Let's get in touch on any of these platforms.
              </Typography.Text>
            </div>
          </Col>
          <Col>
            <Space size={12}>
              <Tooltip placement="top" title="Follow Us">
                <TwitterSquareFilled
                  className={classNames(styles.social, styles.twitter)}
                />
              </Tooltip>
              <FacebookFilled
                className={classNames(styles.social, styles.facebook)}
              />
              <DribbbleSquareFilled
                className={classNames(styles.social, styles.dribbble)}
              />
              <GithubFilled
                className={classNames(styles.social, styles.github)}
              />
            </Space>
          </Col>
        </Row>
        <Divider />
        <Row className={styles.copyright} justify="space-between">
          <Col>
            <strong>
              © 2018 <span className={styles.brand}>Şikayetvar</span>
            </strong>
          </Col>
          <Col>
            <Link to="/" className={styles.link}>
              Posts
            </Link>
          </Col>
        </Row>
      </Container>
    </footer>
  );
};

export default Footer;
