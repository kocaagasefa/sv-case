import { MapContainer, Marker, TileLayer } from "react-leaflet";
import "leaflet/dist/leaflet";
import "./style.css";
const Map = ({ position }) => {
  return (
    <MapContainer
      center={position}
      zoom={4}
      style={{ width: "100%", height: 300 }}
    >
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={position}></Marker>
    </MapContainer>
  );
};

export default Map;
