const ActionCreator = {
  success: (actionType) => `${actionType}_SUCCESS`,
  fail: (actionType) => `${actionType}_FAIL`,
};
export default ActionCreator;
